// Generated by gir (https://github.com/gtk-rs/gir @ d7c0763cacbc)
// from ../../ev-girs (@ 9ed647f47a13+)
// from ../../gir-files (@ 20031a537e40)
// DO NOT EDIT

#include "manual.h"
#include <stdalign.h>
#include <stdio.h>

int main() {
    printf("%s;%zu;%zu\n", "PpsDocumentView", sizeof(PpsDocumentView), alignof(PpsDocumentView));
    printf("%s;%zu;%zu\n", "PpsDocumentViewClass", sizeof(PpsDocumentViewClass), alignof(PpsDocumentViewClass));
    printf("%s;%zu;%zu\n", "PpsFindSidebar", sizeof(PpsFindSidebar), alignof(PpsFindSidebar));
    printf("%s;%zu;%zu\n", "PpsFindSidebarClass", sizeof(PpsFindSidebarClass), alignof(PpsFindSidebarClass));
    printf("%s;%zu;%zu\n", "PpsMessageArea", sizeof(PpsMessageArea), alignof(PpsMessageArea));
    printf("%s;%zu;%zu\n", "PpsMessageAreaClass", sizeof(PpsMessageAreaClass), alignof(PpsMessageAreaClass));
    printf("%s;%zu;%zu\n", "PpsProgressMessageArea", sizeof(PpsProgressMessageArea), alignof(PpsProgressMessageArea));
    printf("%s;%zu;%zu\n", "PpsProgressMessageAreaClass", sizeof(PpsProgressMessageAreaClass), alignof(PpsProgressMessageAreaClass));
    printf("%s;%zu;%zu\n", "PpsSidebarPage", sizeof(PpsSidebarPage), alignof(PpsSidebarPage));
    printf("%s;%zu;%zu\n", "PpsSidebarPageClass", sizeof(PpsSidebarPageClass), alignof(PpsSidebarPageClass));
    return 0;
}
