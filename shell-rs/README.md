# Rust Infrastructure

## Bindings

| directory       | bindings for |
| --------------- | ------------ |
| papers-document | libdocument  |
| papers-view     | libview      |
| papers-shell    | libshell     |

Bindings are generated by  [gir](https://github.com/gtk-rs/gir) and `*.gir` inside `shell-rs/gir-files`. Before generating the bindings, you should checkout the submodule and install `gir`  command.

```
git submodule update --checkout
cd shell-rs/gir
cargo install --path .
```

There is a meson run target named `update-rust-bindings` to update the bindings. The run target depends on the gobject introspection feature and    can be invoked by:

```
meson setup _build -Dintrospection=enabled
meson compile -C _build update-rust-bindings
```

You can also update the bindings through the `shell-rs/update-bindings.sh` script.

## IDE Support

rust-analyzer needs some environment to function properly. The environment can be set by `meson devenv`:

```bash
meson _build
meson devenv -C _build
code ..
```
